#!/usr/bin/ruby

require "rubygems"
require "mysql2"
require "ostruct"
require "YAML"
require "socket"
require "timeout"
require "colorize"
require "digest/md5"
require "json"
require "optparse"
require "pp"

def debug(input)
	puts input
	return
end

class DB_AutoBackup
	#instantiation variables
	attr_accessor :options, :db_config, :debug_flag

	@@mysql_dump_dir = "/tmp/mysql_dumps/"
	@@tunnel_port=10000
	@@server_key_file_prefix = "temp_key_ssh_backup_"
	@@server_key_file_dir = "/tmp/"
	@@db_info = Array.new

	def initialize(options,db_config,debug_flag=true)
		@options = options
		@db_config = db_config
		@debug_flag = debug_flag
		@db_config[:flags] = Mysql2::Client::MULTI_STATEMENTS
		@my = Mysql2::Client.new(db_config)
		self.init_db
	end

	def startBackups
		self.backup_logic @@db_info
	end
		
	def runValidations
		self.validate_log_md5
	end

	#load db info
	def init_db
		#group by should not be used in the case there are multiple users in a database to backup corresponding tables,
		#likely want to use root user to backup --all-databases but should not force this
		load_query = "SELECT *,su.user as server_user,du.user as db_user 
				FROM `servers` s
				LEFT JOIN `server_users` as su 
					USING (server_id)  
				LEFT JOIN `database_users` du 
					USING(server_id) 
				WHERE s.enabled=1
					 GROUP BY s.server_id"

		res = @my.query(load_query)
		res.each do |row|
			info = {
			'hostname' => row['hostname'],
			'server_id' => row['server_id'],
			'port' => row['port'],
			#check ip via ping, if incorrect update db? (hosts file could override this, so maybe a bad idea)
			'ip' => row['ip'],
			'server_user_id' => row['user_id'],
			'server_user' => row['server_user'],
			'server_key' => row['key'],
			'db_user_id' => row['db_user_id'],
			'db_user' => row['db_user'],
			'db_password' => row['password'],
			'db_port' => row['db_port'],
			'db_database' => row['database'],
			}
			@@db_info << info
		end
	end

	#outputs contents of #key into #dir#file then sets permission for use with ssh
	def create_temp_key_file(file,key,dir)
		debug ("Creating key file:\n\t".green+" #{dir}#{file}".blue)  
		IO.popen("echo \"#{key}\" > #{dir}#{file} ; chmod 0600 #{dir}#{file}") do |output|
			while line = output.gets do 
				debug line
			end
		end
	end

	#creates local tunnel (forwards local port #tunnel_port to remote port #port 
	#sleep option at the end of ssh command is required otherwise ssh expects a command to be executed
		#allows for tunnel to be accessible for #sleepz seconds then closes automatically to free port
	#standard mysql dump is then issued via forward local tunnel and output locally
	def create_ssh_tunnel(user,key_file,ip,port,tunnel_port,db_user,db_password,db,local_file,server_id)
		sleepz = 10
		debug "Creating ssh_tunnel:\n\t".green+
			" ssh -f -L#{tunnel_port}:localhost:#{port} #{user}@#{ip} -i #{key_file} sleep #{sleepz}".blue
		host = "127.0.0.1"
		debug "Dumping remote mysql DB:\n\t".green+
			" mysqldump -P #{tunnel_port} -h #{host} -u#{db_user} -p'#{db_password}' #{db} > #{local_file}".blue
		IO.popen(
			"ssh -v -f -L #{tunnel_port}:localhost:#{port} #{user}@#{ip} -i #{key_file} sleep #{sleepz}  ;
			mysqldump -v -P #{tunnel_port} -h #{host} -u#{db_user} -p'#{db_password}' #{db} > #{local_file}"
			) do |output|
			while line = output.gets do 
			#	debug line
			end
		end

		#generate log entry in database with dump info including MD5 of dump file
		md5 = Digest::MD5.file(local_file).hexdigest
		time = Time.now.to_i;	
		file_a = local_file.split('/')
		file = file_a[file_a.length-1]
		file_a[file_a.length-1]=""
		path = file_a.join("/")
		q = "INSERT INTO `backup_log`
			(server_id,create_timestamp,
				modify_timestamp,
				path,
				file,
				host,
				md5)
			VALUES('#{server_id}',#{time},#{time},'#{path}','#{file}','#{host}','#{md5}')"
		@my.query(q)
	end

	#checks if port is available at #ip:#port with connection timeout of #seconds seconds (default 2)
	def is_port_open(ip, port,seconds=2)
	  begin
	    Timeout::timeout(seconds) do
	      begin
		debug "Checking if ip:port is open:\n\t".green+"#{ip}:#{port}".blue
		s = TCPSocket.new(ip, port).close
		debug "\tPort open".green
		return true
	      rescue Errno::ECONNREFUSED, Errno::EHOSTUNREACH
		debug "\tPort unavailable".red
		return false
	      end
	    end
	  rescue Timeout::Error
		debug "\tPort Check Timeout".red
	  end

	  return false
	end
		
	def delete_server server_id
		@my.query( "DELETE FROM servers WHERE server_id=#{server_id}") ;
		debug "Server Deleted."
	end

	def delete_backup(backup_id)
		q= "SELECT * FROM backup_log WHERE backup_id=#{backup_id} LIMIT 1"
		s=@my.query(q)
		s.each do |r|
			file = r['path']+r['file']
			if !File.exists?(file) 
				puts "Error:\t Backup file #{file} not found".red
				return
			end	
			#manual deletion should not delete backup UNLESS master user, request confirmation as well unless force flag specified
			#internal deletion ie. older than 30 days should be deleted, maybe check configuration of some sort
			File.delete(file)
			q = "DELETE FROM backup_log WHERE backup_id=#{backup_id}"
			s1 = @my.query(q)
			data = {'backup_id'=>backup_id,'log_entry'=>r}
			time = Time.now.to_i;	
			self.log_action data.to_json,'backup_deleted',time
		end
	end

	def validate_md5? path, file, md5
		if !md5.length 
			return false
		end
		if md5 != Digest::MD5.file(path+file).hexdigest 
			return false
		end
		return true
	end 
		
	def validate_log_md5 backup_id ="",server_id=""
		q= "SELECT * FROM backup_log "
		if backup_id.length > 0 
			q+= "WHERE backup_id=#{backup_id}"
		end
		if server_id.length > 0 && backup_id.length <=0 
			q+= "WHERE server_id=#{server_id}"
		end
		r = @my.query(q)
		r.each do |log|
			path = log['path']
			file = log['file']
			md5 = log['md5']
			if !md5.length 
				next
			end
			backup_id = log['backup_id']
			if !validate_md5?(path,file,md5)
				time = Time.now.to_i
				stat = File.stat(path+file)
				json = stat.inspect
				data = {'stat'=>json,'backup_id'=>backup_id}
				puts "Error: MD5 validation failed for backup_id #{backup_id}"
				self.log_action data.to_json,'md5_validation_failure',time
			end
		end
	end

	#todo: 
	def validate_backup backup_id
		self.validate_log_md5 backup_id
	end

	#todo: 
	def validate_server server_id
		self.validate_log_md5 "",server_id
	end

	def log_action data,type,timestamp
		q1 = "INSERT into `action_log`	
			(data,type,timestamp)
			VALUES('#{data}','#{type}',#{timestamp})"
		@my.query(q1)
	end
	
	def restore_remote_db_from_backup backup_id
		q = "SELECT * FROM backup_log 
			LEFT JOIN servers 
				USING(server_id)
			LEFT JOIN server_users 
				USING(server_id)
			LEFT JOIN database_users	
				USING(server_id)
				WHERE `servers`.backup_id=#{backup_id}"
		r = @my.query(q) 	
	end

	def backup_logic db_i
		data = []
		db_i.each do |info|
			#local filename to store db
			server_key_file = @@server_key_file_prefix+"#{info['server_id']}"
			#create temporary key file 
			self.create_temp_key_file(server_key_file,
						info['server_key'],
						@@server_key_file_dir)
			@@tunnel_port = @@tunnel_port+info['server_id']
			key_file = @@server_key_file_dir+server_key_file
			time = Time.new
			stime = time.strftime("%Y-%m-%d_%H:%M:%S")
			local_db_dump = "mysql_backup_#{info['server_id']}_#{stime}"
			#find open port to create tunnel with
			while self.is_port_open("localhost",@@tunnel_port) do 
				debug "Checking port:".green+" #{@@tunnel_port}".blue
				@@tunnel_port+=1
			end
			self.create_ssh_tunnel(info['server_user'],
						@@server_key_file_dir+server_key_file,
						info['ip'],
						info['db_port'],
						@@tunnel_port,
						info['db_user'],
						info['db_password'],
						info['db_database'],
					@@mysql_dump_dir+local_db_dump,
						info['server_id'])

			data << info 
		end
		data = {'data' => data }
		log_action data.to_json,'all_servers_backed_up',Time.now.to_i
	end
	
	#check server exists by server_id
	def check_server_exists server_id
		q = "SELECT count(*) as num  FROM servers where server_id=#{server_id}"
		r = self.query(q)
		r.each do |s| 
			if s['num'] >= 1 
				return true
			else	
				return false
			end
		end
	end

	#check server exists by backup_id
	def check_backup_exists backup_id
		q = "SELECT count(*) FROM backup_log where backup_id=#{backup_id}"
		r = self.query(q)
		if r >= 1 
			return true
		else	
			return false
		end
	end

	#logic to add a server 
	def add_server hostname,ip,ssh_port,db_port,enabled=1
		if ssh_port.to_f!=ssh_port.to_i || db_port.to_f!=db_port.to_i || !hostname.length || !ip.length 
			#throw error and add to action log
			debug "Error: Invalid input"
			return false
		end
		q = "INSERT INTO `servers` (`hostname`,`ip`,`port`,`db_port`,`enabled`) 
			VALUES ('#{hostname}','#{ip}','#{ssh_port}','#{db_port}','#{enabled}') "
		r = self.query(q)
		if r===false #if successful, return value will be nil 
			#throw error and add to action log
			p r
			debug "Error: Insert failed check input (#{q}"
			return false
		end
		last_id = self.last_id
		debug "Server Inserted: #{last_id}"
		return last_id
	end
	
	#logic to add a server user
	def add_server_user server_id,user,key
		if server_id.to_f!=server_id.to_i || !user.length || !key.length 
			#throw error and add to action log
			debug "Error: Invalid input"
			return false
		end
		q = "INSERT INTO `server_users` (`server_id`,`user`,`key`) 
			VALUES (#{server_id},'#{user}','#{key}') "
		r = self.query(q)
		if r===false #if successful, return value will be nil 
			#throw error and add to action log
			p r
			debug "Error: Insert failed check server id (#{q}"
			return false
		end
		last_id = self.last_id
		debug "Server User Inserted: #{last_id}"
		return last_id
	end

	#logic to add a database user
	def add_database server_id,user,password,database='--all-databases'
		if server_id.to_f!=server_id.to_i || !user.length || !password.length || !database.length
			#throw error and add to action log
			debug "Error: Invalid input"
			return false
		end
		q = "INSERT INTO `database_users` (`server_id`,`user`,`password`,`database`) 
			VALUES (#{server_id},'#{user}','#{password}','#{database}') "
		r = self.query(q)
		if r===false #if successful, return value will be nil 
			#throw error and add to action log
			p r
			debug "Error: Insert failed check server id (#{q}"
			return false
		end
		last_id = self.last_id
		debug "Database User Inserted: #{last_id}"
		return last_id
	end

	#returns last id for mysql query
	def last_id
		return @my.last_id
	end

	def query string
		begin
			r = @my.query(string)
			return r
		rescue Mysql2::Error
			debug "Error: Mysql Query Failed (#{$!})"
			return $!
		end
	end

	#todo: 
	def backup_server server_id
		load_query = "SELECT *,su.user as server_user,du.user as db_user 
				FROM `servers` s
				LEFT JOIN `server_users` as su 
					USING (server_id)  
				LEFT JOIN `database_users` du 
					USING(server_id) 
				WHERE s.enabled=1 AND s.server_id=#{server_id}
					 GROUP BY s.server_id 
				LIMIT 1"

		res = @my.query(load_query)
		res.each do |row|
			info = {
			'hostname' => row['hostname'],
			'server_id' => row['server_id'],
			'port' => row['port'],
			#check ip via ping, if incorrect update db? (hosts file could override this, so maybe a bad idea)
			'ip' => row['ip'],
			'server_user_id' => row['user_id'],
			'server_user' => row['server_user'],
			'server_key' => row['key'],
			'db_user_id' => row['db_user_id'],
			'db_user' => row['db_user'],
			'db_password' => row['password'],
			'db_port' => row['db_port'],
			'db_database' => row['database'],
			}
			self.backup_logic info
		end
	end

	def list_servers 
		q = "SELECT * FROM servers"
		res = @my.query(q)
		res.each do |r|
			pp r
		end
	end

	#todo: 
	def restore_backup backup_id

	end

	#todo: 
	def restore_latest_backup server_id
		r = "SELECT backup_id FROM backup_log 
			WHERE server_id=#{server_id} 
			ORDER BY create_timestamp 
			DESC LIMIT 1"
		backup_id = @my.query(r)
		if backup_id == nil
			debug "No backup found"
			exit;
		end
		self.restore_remote_db_from_backup backup_id
	end

	#setup mysql database for autobackup use
	def setup_database
setup = "
DROP TABLE IF EXISTS `action_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_log` (
  `data` longtext NOT NULL,
  `action_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` enum('backup_deleted','full_log_md5_validation','md5_validation_failure','all_servers_backed_up','single_server_backed_up','single_server_restored','single_server_exported') NOT NULL,
  `timestamp` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`action_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_user_access`
--

DROP TABLE IF EXISTS `admin_user_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user_access` (
  `server_id` bigint(20) NOT NULL,
  `admin_user_id` bigint(20) NOT NULL,
  `create_timestamp` int(11) NOT NULL,
  `level` enum('all','read','write') NOT NULL,
  KEY `server_id` (`server_id`,`admin_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `admin_user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` int(11) NOT NULL,
  `password_hash` int(11) NOT NULL,
  `create_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`admin_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `servers`
--

DROP TABLE IF EXISTS `servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servers` (
  `server_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `port` int(5) NOT NULL,
  `db_port` int(11) NOT NULL DEFAULT '3306',
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`server_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
--
-- Table structure for table `backup_log`
--

DROP TABLE IF EXISTS `backup_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `backup_log` (
  `backup_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `server_id` bigint(20) NOT NULL,
  `create_timestamp` int(11) NOT NULL,
  `modify_timestamp` int(11) NOT NULL,
  `path` text NOT NULL,
  `file` varchar(255) NOT NULL,
  `host` varchar(255) NOT NULL,
  `md5` varchar(255) NOT NULL,
  PRIMARY KEY (`backup_id`),
  KEY `server_id` (`server_id`),
  CONSTRAINT `backup_log_ibfk_1` FOREIGN KEY (`server_id`) REFERENCES `servers` (`server_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `database_users`
--

DROP TABLE IF EXISTS `database_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `database_users` (
  `db_user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `server_id` bigint(20) NOT NULL,
  `user` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `database` varchar(255) NOT NULL DEFAULT '--all-databases',
  PRIMARY KEY (`db_user_id`),
  KEY `server_id` (`server_id`),
  CONSTRAINT `database_users_ibfk_1` FOREIGN KEY (`server_id`) REFERENCES `servers` (`server_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `server_users`
--

DROP TABLE IF EXISTS `server_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `server_users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `server_id` bigint(20) NOT NULL,
  `user` varchar(255) NOT NULL DEFAULT 'root',
  `key` longtext NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `server_id` (`server_id`),
  CONSTRAINT `server_users_ibfk_1` FOREIGN KEY (`server_id`) REFERENCES `servers` (`server_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

"
	@my.query(setup)
	end

end


#CODES = %w[iso-2022-jp shift_jis euc-jp utf8 binary]
#CODE_ALIASES = { "jis" => "iso-2022-jp", "sjis" => "shift_jis" }

# The options specified on the command line will be collected in *options*.
    # We set default values here.
    options = OpenStruct.new
    options.library = []
    options.inplace = false
    options.encoding = "utf8"
    options.transfer_type = :auto
    options.verbose = false


usage = <<eos
		Functionality ::::
		
			
		Restore latest backup remotely
			ruby init.rb restore-latest-backup [server id]
				ex: ruby init.rb restore-backup 1
			Note :: this will backup current remote database prior to overwriting with backup.  Specify option -f if would like to skip this and force immediate restoration
			Outputs :: success or detailed failure message

		Restore backup remotely
			ruby init.rb restore-backup [backup id]
				ex: ruby init.rb restore-backup 1
			Note :: this will backup current remote database prior to overwriting with backup.  Specify option -f if would like to skip this and force immediate restoration
			Outputs :: success or detailed failure message

		Validate Specific Backup (compares MD5 checksum stored on backup with filesystem checksum. Reports and logs any inconsistencies) 
			ruby init.rb validate-backup [backup id]
				ex: ruby init.rb validate-backup 1
			Outputs :: success or detailed failure message

		Validate Backups For specific Server (compares MD5 checksum stored on backup with filesystem checksum. Reports and logs any inconsistencies) 
			ruby init.rb validate-server [server id]
				ex: ruby init.rb validate-server 1
			Outputs :: success or detailed failure message

		Validate All Backups (compares MD5 checksum stored on backup with filesystem checksum. Reports and logs any inconsistencies) 
			ruby init.rb validate-all
				ex: ruby init.rb validate-all
			Outputs :: success or detailed failure message

		Backup All Servers (useful as cronjob) 
			ruby init.rb backup-all
				ex: ruby init.rb backup-all
			Outputs :: detailed success including backup ids or failure message

		Backup Specific Server 
			ruby init.rb backup-server [server id]
				ex: ruby init.rb backup-server 1
			Outputs :: success + backup id or failure message

		Deleting a specific backup
			ruby init.rb delete-backup [backup id]
				ex: ruby init.rb delete-backup 5 
			Outputs :: success or failure message
			Note: This will delete file backup, so use carefully.

		Adding a database user account to backup database: 
			ruby init.rb add-database [server id] [user account] [password] [database (optional, if ommited, all databases will be backed up)]
				ex: ruby init.rb add-database 5 root root database 
			Outputs :: database user id

		Adding a Server to backup database: 
			ruby init.rb add-server [host] [ip address] [server ssh port] [server mysql port]
				ex: ruby init.rb add-server 'google.com' '8.8.8.8'  '22' '3306'
			Outputs :: server id [required to add user account or db user account, so write this value down]

		Adding a Server User account to backup database: 
			cat [key_file] > ruby init.rb add-server-user [server id]  [user name]
				ex: cat ~/.ssh/id_rsa > ruby init.rb add-server-user 5 root
			Outputs :: server user id
eos

OptionParser.new do |opts|
	opts.banner = "Usage init.rb [options]"+"\n"+usage
	opts.on("-f", "--force ","Forcefully Delete Backup") do |v|
		options[:force] = v
	end

	opts.separator ""
	opts.separator "Common options:"
	opts.on("-v", "--[no]verbose","Run verbosely") do |v|
		options[:verbose] = v
	end
	opts.on("-s", "--silent","Run silently") do |v|
		options[:silent] = v
	end
	
	opts.on_tail("-h", "--help", "Show this message") do
        puts opts
        exit
      end

      # Another typical switch to print the version.
      opts.on_tail("--version", "Show version") do
        puts OptionParser::Version.join('.')
        exit
      end
	
end.parse!

def handle_input 
current_option = ARGV[0]
case current_option 
	when "backup-server"
		server_id = ARGV[1]
		if server_id == nil
			p "You must enter a server id to backup"
			exit;
		end
		if server_id.to_i != server_id.to_f 
			p "Please enter a numeric value for the server id"
			exit;
		end
		if !Backup.check_server_exists(server_id)
			p "Server: #{server_id} not found".red
			exit;
		end
		Backup.backup_server server_id	
	when "backup-all"
		p "Backing up all databases...".green
		Backup.startBackups
	when "validate-backup"
		backup_id = ARGV[1]
		if backup_id == nil
			p "You must enter a backup id to validate"
			exit;
		end
		if backup_id.to_i != backup_id.to_f 
			p "Please enter a numeric value for the backup id"
			exit;
		end
		if !Backup.check_backup_exists(backup_id)
			p "Backup: #{backup_id} not found".red
			exit;
		end
		Backup.validate_backup backup_id	
	when "validate-server"
		server_id = ARGV[1]
		if server_id == nil
			p "You must enter a server id to validate"
			exit;
		end
		if server_id.to_i != server_id.to_f 
			p "Please enter a numeric value for the server id"
			exit;
		end
		if !Backup.check_server_exists(server_id)
			p "Server: #{server_id} not found".red
			exit;
		end
		Backup.validate_server server_id	
	when "validate-all"
		p "Validating all database backups...".green
		Backup.runValidations
	when "restore-backup"
		backup_id = ARGV[1]
		if backup_id == nil
			p "You must enter a backup id to validate"
			exit;
		end
		if backup_id.to_i != backup_id.to_f 
			p "Please enter a numeric value for the backup id"
			exit;
		end
		if !DB_AutoBackup.check_backup_exists(backup_id)
			p "Backup: #{backup_id} not found".red
			exit;
		end
		Backup.restore_remote_db_from_backup backup_id	
	when "restore-latest-backup"
		server_id = ARGV[1]
		if server_id == nil
			p "You must enter a server id to validate"
			exit;
		end
		if server_id.to_i != server_id.to_f 
			p "Please enter a numeric value for the server id"
			exit;
		end
		if !Backup.check_server_exists(server_id)
			p "Server: #{server_id} not found".red
			exit;
		end
		Backup.restore_latest_backup backup_id	
	when "delete-server"
		server_id= ARGV[1]
		if !Backup.check_server_exists(server_id)
			p "Server: #{server_id} not found".red
			exit;
		end
		puts "Are you sure you want to delete server #{server_id}? (y/n)"
		boo = $stdin.gets
		if boo=="n" || boo=="N"
			exit;
		end	
		if boo=="y" || boo=="Y"
			Backup.delete_server server_id
		end
	when "delete-backup"
		backup_id = ARGV[1]
		if backup_id == nil
			p "You must enter a backup id to delete"
			exit;
		end
		if backup_id.to_i != backup_id.to_f 
			p "Please enter a numeric value for the backup id"
			exit;
		end
		if !DB_AutoBackup.check_backup_exists(backup_id)
			p "Backup: #{backup_id} not found".red
		end
		force = options[:force]
		Backup.delete_backup backup_id,force
	when "add-database"
		server_id = ARGV[1]
		username = ARGV[2]
		password = ARGV[3]
		database = ARGV[4]
		if server_id == nil || !Backup.check_server_exists(server_id)
			p "Server: #{server_id} not found".red
			exit;
		end
		if username.length <=0 
			p "Enter a valid username"
			exit;
		end
		if password.length <=0 
			p "Enter a valid password"
			exit;
		end
		if database == nil
			database = '--all-databases'
		end
		Backup.add_database_user server_id, username, password, database 
	when "add-server-user"
		server_id = ARGV[1]
		username = ARGV[2]
		key = $stdin.read
		if server_id == nil || !Backup.check_server_exists(server_id)
			p "Server: #{server_id} not found".red
			exit;
		end
		if username.length <=0 
			p "Enter a valid username"
			exit;
		end
		if key.length <=0 
			p "Enter a valid password"
			exit;
		end
		if server_id == nil || !Backup.check_server_exists(server_id)
			p "Server: #{server_id} not found".red
		end
		Backup.add_server_user server_id,username,key	
	when "add-server"
		host = ARGV[1]
		ip = ARGV[2]
		ssh_port = ARGV[3]
		mysql_port = ARGV[3]
		Backup.add_server host, ip , ssh_port, mysql_port
	when "install"
		Backup.setup_database
	when "list-servers"
		Backup.list_servers
	else
		p "please enter a valid command"
end
end

db = {:host => "localhost",
			:username => "root",
			:password => "root",
			:database => "backup"}
Backup = DB_AutoBackup.new(options,db)
handle_input()
#Backup.startBackups
#Backup.runValidations
#Backup.add_database_user 5,'root','password'
#key = $stdin.read
#p key  
#Backup.add_server_user 5,'root',key
#Backup.add_server 'google.com','8.8.8.8' ,'22','3306'
#Backup.delete_backup(13)
